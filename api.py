from flask import Flask, request
import json
app = Flask(__name__)

@app.route('/trump', methods=['POST'])
def post_trump():
    request_dict = json.loads(request.get_data())
    message = request_dict["item"]["message"]["message"]
    message_array = message.split()
    message_array.pop(0)
    message = " ".join(message_array).strip()
    if len(message) < 1:
        message = "America"
    dict = {
        "color": "green",
        "message": "(trump) Make " + message + " great again!",
        "notify": False,
        "message_format": "text"
    }
    return json.dumps(dict)